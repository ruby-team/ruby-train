ruby-train (3.12.6-1) unstable; urgency=medium

  * New upstream version 3.12.6
  * d/watch: follow tags instead of releases on Github
  * Drop 0002-remove-version-cap-on-net-ssh-and-net-scp.patch
  * Add 0002-drop-spec-constraint-on-ffi.patch
    + this is a transitive dependency, linux not affected by the constaint
  * Add upstream contact in d/copyright

 -- Cédric Boutillier <boutil@debian.org>  Mon, 22 Jul 2024 00:16:14 +0200

ruby-train (3.2.28-3) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-addressable, ruby-json,
      ruby-mixlib-shellout, ruby-net-scp and ruby-net-ssh.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Cédric Boutillier ]
  * Remove X?-Ruby-Versions fields from d/control
  * Bump Standards-Version to 4.6.2 (no changes needed)
  * Remove dependency on byebug gem in tests
  * Do not build-depend on ruby-byebug

 -- Cédric Boutillier <boutil@debian.org>  Thu, 23 Nov 2023 22:50:03 +0100

ruby-train (3.2.28-2) unstable; urgency=medium

  * Revert my undoing of Unit 193's changes in their previous upload.

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 09 May 2020 11:48:26 -0300

ruby-train (3.2.28-1) unstable; urgency=medium

  * New upstream version 3.2.28
  * Refresh packaging files with new run of dh-make-ruby
  * Drop patch, fixed upstream
  * Add patch to relax dependencies

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 08 May 2020 18:26:30 -0300

ruby-train (3.2.20-3) unstable; urgency=medium

  * Team upload.
  * d/p/0002-remove-version-cap-on-net-ssh-and-net-scp.patch:
    - Remove version cap on net-ssh and net-scp.

 -- Unit 193 <unit193@debian.org>  Mon, 04 May 2020 20:15:51 -0400

ruby-train (3.2.20-2) unstable; urgency=medium

  * Source only upload

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 09 Mar 2020 16:03:04 -0300

ruby-train (3.2.20-1) unstable; urgency=medium

  * Initial release

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 06 Mar 2020 18:10:58 -0300
