require 'gem2deb/rake/testtask'

reject = Dir['test/**/*_test.rb']
  .grep(%r{transports/(azure|clients|docker|gcp|helpers|vmware)})
reject += Dir['test/integration/**/*.rb']
reject += Dir['test/windows/**/*.rb']

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.test_files = ['test/helper.rb'] + FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb'] - reject
end
